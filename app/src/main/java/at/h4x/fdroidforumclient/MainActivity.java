package at.h4x.fdroidforumclient;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private GenericWebViewClient webViewClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setup titlebar if disabled
        if (getResources().getBoolean(R.bool.ui_titlebar)) {
            getSupportActionBar().show();
        } else {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.activity_main);

        webView = findViewById(R.id.webview);

        webViewClient = new GenericWebViewClient(this);
        webView.setWebViewClient(webViewClient);

        WebSettings webSettings = webView.getSettings();
        webSettings.setAllowContentAccess(getResources().getBoolean(R.bool.webview_allow_content_access));
        webSettings.setAllowFileAccess(getResources().getBoolean(R.bool.webview_allow_file_access));
        webSettings.setAllowFileAccessFromFileURLs(getResources().getBoolean(R.bool.webview_allow_file_access_from_file_urls));
        webSettings.setAllowUniversalAccessFromFileURLs(getResources().getBoolean(R.bool.webview_allow_universal_access_from_file_urls));
        webSettings.setAppCacheEnabled(getResources().getBoolean(R.bool.webview_app_cache_enabled));
        webSettings.setDatabaseEnabled(getResources().getBoolean(R.bool.webview_database_enabled));
        webSettings.setJavaScriptCanOpenWindowsAutomatically(getResources().getBoolean(R.bool.webview_javascript_can_open_windows_automatically));
        webSettings.setJavaScriptEnabled(getResources().getBoolean(R.bool.webview_javascript_enabled));
        webSettings.setSupportZoom(getResources().getBoolean(R.bool.webview_zoom_enabled));

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                if (BuildConfig.DEBUG) {
                    Log.i("###", "webView console: [" + consoleMessage.sourceId() + ":" + consoleMessage.lineNumber() + "]" + consoleMessage.message());
                    return true;
                } else {
                    return false;
                }
            }
        });

        webView.loadUrl(getResources().getString(R.string.home_url));
    }

    @Override
    protected void onPause() {
        webView.onPause();
        webView.pauseTimers();
        super.onPause();
    }

    @Override
    protected void onResume() {
        webView.onResume();
        webView.resumeTimers();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        webView.destroy();
        webView = null;
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (getString(R.string.home_url).equals(webView.getUrl())) {
            super.onBackPressed();
        } else {
            webView.evaluateJavascript("$(\"#site-logo\").click();", new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                }
            });
        }
    }
}
