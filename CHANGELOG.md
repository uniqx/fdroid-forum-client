<!--
SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.1] - 2020-08-01
### Fixed
- german app name store listing translation
- fixed typo in store listing text

### Added
- started keeping a changelog

## [v1.0.0] - 2020-07-20
### Added
- inital release

[Unreleased]: https://codeberg.org/uniqx/MetaApp/src/branch/master
[v1.0.1]: https://codeberg.org/uniqx/MetaApp/src/tag/v1.0.1
[v1.0.0]: https://codeberg.org/uniqx/MetaApp/src/tag/v1.0.0
